const Sequelize = require('sequelize')
const fs = require('fs')
const path = require('path')
const basename = path.basename(__filename)
const modelsPath = path.join(__dirname, '../models')
let db = {}
let sequelize

sequelize = new Sequelize('imagespark', 'echagirov', 'password', {
  host: '127.0.0.1',
  dialect: 'mysql',
  define: {
    charset: 'utf8mb4',
    collate: 'utf8mb4_general_ci',
    timestamps: false,
  },
})

// Загрузка моделей
fs.readdirSync(modelsPath)
  .filter(file => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(modelsPath, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
