const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const indexRouter = require('./routes/index')
const userRouter = require('./routes/user')
const playerRouter = require('./routes/player')
const app = express()
const _ = require('lodash')

app.use(cors())
app.use(bodyParser.json({ limit: '50mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

app.use(express.static('public'))

app.use('/', indexRouter)
app.use('/user', userRouter)
app.use('/player', playerRouter)

app.use(function(err, req, res, next) {
  let errors = _(err.errors || err).map(error => ({
    id: error.id,
    message: error.message,
  }))

  res.status(200).json({ errors })
})

module.exports = app
