const Player = require('../db').Player
const Jimp = require('jimp')

exports.playerAdd = (req, res, next) => {
  if (req.body.image) {
    const base64 = req.body.image.replace(/^data:image\/[a-z]+;base64,/, '')
    const buffer = Buffer.from(base64, 'base64')

    Jimp.read(buffer, (err, image) => {
      if (err) throw err

      image
        .quality(80)
        .getBase64(Jimp.MIME_JPEG, function(err, src) {
          console.log(src)
        })
        .write(`public/images/${req.body.imageName}`)
    })
  }

  Player.create({
    name: req.body.name,
    secondName: req.body.secondName,
    age: req.body.age,
    position: req.body.position,
    rating: req.body.rating,
    avatar: req.body.imageName,
  })
    .then(player => {
      console.log(player)
      res.status(200).json(player)
    })
    .catch(err => {
      console.log(err)
      next(err)
    })
}

exports.playerGetAll = (req, res, next) => {
  Player.findAll()
    .then(players => {
      console.log(players)
      res.status(200).json(players)
    })
    .catch(err => {
      console.log(err)
      next(err)
    })
}
