const User = require('../db').User

exports.userReg = (req, res, next) => {
  User.create({ login: req.body.login, password: req.body.password })
    .then(user => {
      console.log(user)
      res.status(200).json(user)
    })
    .catch(err => {
      console.log(err)
      next(err)
    })
}

exports.userAuth = (req, res, next) => {
  User.findOne({ where: { login: req.body.login, password: req.body.password } })
    .then(user => {
      if (!user) throw new Error('Пользователь не найден')
      console.log(user)
      res.status(200).json(user)
    })
    .catch(err => {
      console.log(err)
      let error = {
        id: 'user_not_found',
        message: err.message,
      }
      next([error])
    })
}
