const express = require('express')
const router = express.Router()

const userControllers = require('../controllers/userControllers')

router.post('/auth', userControllers.userAuth)
router.post('/register', userControllers.userReg)

module.exports = router
