const express = require('express')
const router = express.Router()

const playerControllers = require('../controllers/playerControllers')

router.post('/add', playerControllers.playerAdd)
router.get('/get_all', playerControllers.playerGetAll)

module.exports = router
