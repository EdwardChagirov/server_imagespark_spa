'use strict'
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    'User',
    {
      login: Sequelize.STRING,
      password: Sequelize.STRING,
    },
    {},
  )
  // eslint-disable-next-line no-unused-vars
  User.associate = function(models) {
    // associations can be defined here
  }
  return User
}
