'use strict'
module.exports = (sequelize, DataTypes) => {
  const Player = sequelize.define(
    'Player',
    {
      name: DataTypes.STRING,
      secondName: DataTypes.STRING,
      age: DataTypes.INTEGER,
      position: DataTypes.INTEGER,
      rating: DataTypes.INTEGER,
      avatar: DataTypes.STRING,
    },
    {},
  )
  Player.associate = function(models) {
    // associations can be defined here
  }
  return Player
}
